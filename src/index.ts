type PlayerScore = [string, number]

type LeaderBoard = {[key:string]: PlayerScore[]}

async function handleFiles(this: HTMLInputElement) {
  if (this.files === null) {
    return
  }
  const csvContent = await this.files[0].text()
  const csvLines = csvContent.split(/\r?\n/);
  const leaderBoardSections = csvLines[0].split(';').slice(1);

  const leaderBoard: LeaderBoard = {}
  csvLines.slice(1).filter((c) => c).forEach((csvLine) => {
    const csvLineTab = csvLine.split(';');
    csvLineTab.slice(1).forEach((score: string, index: number) => {
      if (typeof leaderBoard[leaderBoardSections[index]] === 'undefined') {
        leaderBoard[leaderBoardSections[index]] = []
      }
      leaderBoard[leaderBoardSections[index]].push([csvLineTab[0], Number(score)])
    })
  });
  Object.keys(leaderBoard).forEach((k) => leaderBoard[k].sort((a, b) => b[1] - a[1]))

  const formEl = document.getElementById("form");
  if (formEl !== null) {
    formEl.hidden = true;
  }

  const leaderBoardEl = document.getElementById("leaderboard");
  if (leaderBoardEl === null) {
    return
  }

  Object.keys(leaderBoard).forEach((k) => {
    const leaderBoardSectionEl = document.createElement("section");
    const leaderBoardTitleEl = document.createElement("h1");
    leaderBoardTitleEl.textContent = k;
    leaderBoardSectionEl.appendChild(leaderBoardTitleEl);

    const scoreListEl = document.createElement("ul");
    leaderBoard[k].slice(0, 3).forEach(([player, score]) => {
      const scoreListItemEl = document.createElement("li");
      scoreListItemEl.textContent = `${player} -> ${score}`;
      scoreListEl.appendChild(scoreListItemEl);
    });

    leaderBoardEl.append(leaderBoardSectionEl);
    leaderBoardEl.append(scoreListEl);
  })
}

document.addEventListener("DOMContentLoaded", function () {
  const inputElement = document.getElementById("csv");

  if (inputElement !== null) {
    inputElement.addEventListener("change", handleFiles);
  }
});
